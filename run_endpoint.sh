#!/usr/bin/env bash

# Extra debugging ?
set -x
set -o nounset

DRAFT=29
HQ_CLI=/proxygen/proxygen/_build/proxygen/httpserver/hq
PORT=4433
LOGLEVEL=2

# Unless noted otherwise, test cases use HTTP/0.9 for file transfers.
PROTOCOL="hq-${DRAFT}"
HTTPVERSION="0.9"

# Default enormous flow control.

EARLYDATA="false"
PSK_FILE="" # in memory psk


echo "Running QUIC server on 0.0.0.0:${PORT}"
${HQ_CLI} \
    --mode=server \
    --port=${PORT} \
    --httpversion=${HTTPVERSION} \
    --h2port=${PORT} \
    --static_root=/www \
    --logdir=/logs \
    --qlogger_path=/logs \
    --host=172.17.0.2 \
    --congestion=bbr \
    --pacing=true \
    --v=${LOGLEVEL} 2>&1 | tee /logs/server.log
